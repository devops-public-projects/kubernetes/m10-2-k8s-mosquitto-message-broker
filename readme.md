
# Deploy Mosquitto Message Broker with Kubernetes
Deploy Mosquitto message broker with ConfigMap and Secret Volume Types.

## Technologies Used
- Kubernetes
- Docker
- Mosquitto

## Project Description
- Define configuration and passwords for Mosquitto message broker with ConfigMap and Secret Volume types

## Prerequisites
- [Minikube](https://minikube.sigs.k8s.io/docs/start/) 
- [Kubectl](https://kubernetes.io/docs/tasks/tools/)
- [Docker](https://docs.docker.com/engine/install/)
- [Starting Code](https://gitlab.com/twn-devops-bootcamp/latest/10-kubernetes/configmap-and-secret-volume-types/-/tree/starting-code?ref_type=heads)

## Guide Steps
- Start Minikube if not already done
	-  `minikube start`

### Mosquitto without Volumes
- Start the pod
	- `kubectl apply -f mosquitto-without-volumes.yaml`
- Verify Pod
	- `kubectl get pod`
- Enter Pod
	- `kubectl exec -it POD_NAME -- /bin/sh`
- Check Default Config File
	- `cd mosquitto/config`
	- `cat mosquitto.conf`
- Delete Pod
	- `kubectl delete -f mosquitto-without-volumes.yaml`

### Mosquitto with Volumes | ConfigMap and Secret

***Note***: ConfigMap and Secret must be created before the Pod starts
- Create ConfigMap and Secret
	- `kubectl apply -f config-file.yaml`
	- `kubectl apply -f secret-file.yaml`
- Verify Creation
	- `kubectl get configmap`
	- `kubectl get secret`
#### Modify the mosquitto-without-volumes.yaml to allow to use volumes
***Note:*** We will use the volume attribute at the containers level

```yaml
    spec:
        containers:
          - name: mosquitto
            image: eclipse-mosquitto:2.0
            ports:
              - containerPort: 1883
# Above is old, below is new
            volumeMounts: 
              - name: mosquitto-config
                mountPath: /mosquitto/config
              - name: mosquitto-secret
                mountPath: /mosquitto/secret
                readOnly: true
        volumes:
          - name: mosquitto-config
            configMap:
              name: mosquitto-config-file
          - name: mosquitto-secret
            secret:
              secretName: mosquitto-secret-file
```
- We added a volumeMounts in order to mount the volumes (ConfigMap and Secret) that we added before.
- There are two sections, first to add the volumes to the Pod, second to mount the Volumes so the Container in our Pod as access them.

#### Run and Check Mosquitto Pod
- Run the Pod
	-  `kubectl apply -f mosquitto.yaml`
- Verify Pod
	- `kubectl get pod`
- Enter Pod
	- `kubectl exec -it POD_NAME -- /bin/sh`
- Verify our Volume Modifications
	- `cd mosquitto`
- We can now see the previous **config** directory and a new **secret** directory
	- See the contents of our **config-file.yaml**
		- `cat /config/mosquitto.conf`
	- See the contents of our **secret-file.yaml**
		- `cat /secret/secret.file`